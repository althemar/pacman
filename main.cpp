
#include <iostream>
#include <utility>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <fstream>
#include <deque>
#include <vector>

#include "View/Ecran.hpp"

using namespace std;

int main(){
  	
  srand (time(NULL));

  /*
  Plateau * p = new Plateau(11, 15);
  p->afficherTerminal();
  cout<<"\nDelete Ligne\n"<<endl;
  p->supprimerLigne(2);
  p->afficherTerminal();
  */
  
    Ecran* ecran = new Ecran(L_ECRAN, H_ECRAN);
  
    ecran->afficher();
    delete ecran;
  
  
  // Nécéssaire pour supprimer le SDL_Event
  Controller::clearEvent();
  
  return 0;
}
