#ifndef PLATEAU
#define PLATEAU


#include <list>
#include "Fantome.hpp"
#include "Case.hpp"
#include "Mur.hpp"
#include "Passage.hpp"
#include "Portail.hpp"
#include "PorteFantomes.hpp"
#include "MaisonFantomes.hpp"

#include <deque>



enum TypePlateau{plateauAccueil, plateauPartie};

class Plateau {

  int hauteur;
  int largeur;
  int tailleCase;
  int nbFantomesDansMaison;
  
  std::deque<std::deque<Case*> > tab_cases;
  Pacman* pacman;
  std::deque<Fantome*> fantomes;

  Passage * caseFruit;
  PorteFantomes * caseSortieMaison;

public:
  Plateau(int hauteur, int largeur);
  Plateau(std::string fichier);
  ~Plateau();

  void chargerFichierPlateau(std::string fichier);
  void ecrireFichierPlateau(std::string fichier);
  int getLargeur();
  int getHauteur();
  int getNbFantomesDansMaison();
  Case* getCase(int i, int j);

  int getTailleCase();

  void editerCase(int x, int y, Case * newCase);
  Pacman* getPacman();

  int getScore();

  void calculTailleCase();
  void setScore(int score);
  int nbPacgum();
  Passage * getCaseFruit();

  std::deque<Fantome*> getFantomes();

  void retirerPacman();

  Case * placerPacman(int posX, int posY);
  
  bool verifierCollisionsFantomes();
  void libererFantome();

  void associerCases();

  void afficherTerminal();
  void PlateauRandom();
  
  void ajouterLigneHaut();
  void ajouterLigneBas();
  void ajouterColonneGauche();
  void ajouterColonneDroite();

  void supprimerLigne(int num);
  void supprimerColonne(int num);

  void placerFruit(TypeObjet fruit);
  
  bool estValide();
  bool validationRebord(int i, int j);
  bool validationPassage(int i, int j);
  bool validationUniqueChemin();
  bool validationMaisonFantomes(int x, int y);
  bool isCaseType(int x, int y, TypeCase type);

  void placerMaisonFantomes(int x, int y);
  void supprimerMaisonFantomes();
  bool isInMaisonFantomes(int x, int y);
  void placerFantomes(int x, int y);
};

  
#endif
