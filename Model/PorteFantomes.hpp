#ifndef PORTE_FANTOMES
#define PORTE_FANTOMES

#include "Case.hpp"

class PorteFantomes : public Case{
	
public: 

  PorteFantomes(int tailleCase);
  PorteFantomes(int tailleCase, int posTableauX, int posTableauY);
  ~PorteFantomes();
  
  bool isMur();
  TypeCase getType();
  Objet * getContenant();
  void afficherTerminal();
  
};

  
#endif
