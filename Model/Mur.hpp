#ifndef MUR
#define MUR

#include "Case.hpp"

class Mur: public Case {
  
public: 

  Mur(int tailleCase);
  Mur(int tailleCase, int posTableauX, int posTableauY);
  ~Mur();
  
  Mur();
  bool isMur();
  TypeCase getType();
  void afficherTerminal();
  
  TypeCase getTypeMur0();
  TypeCase getTypeMur1();
  TypeCase getTypeMur2();
  TypeCase getTypeMur3();
  TypeCase getTypeMur4();
  
  Objet * getContenant();

  int getNbDiago();
};

#endif
