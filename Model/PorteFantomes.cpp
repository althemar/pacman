#include "PorteFantomes.hpp"

using namespace std;

PorteFantomes::PorteFantomes(int tailleCase)
  : Case(tailleCase)
{
}

PorteFantomes::PorteFantomes(int tailleCase, int posTableauX, int posTableauY)
  : Case(tailleCase, posTableauX, posTableauY)
{
}

PorteFantomes::~PorteFantomes(){
}

bool PorteFantomes::isMur(){
  return false;
}

TypeCase PorteFantomes::getType(){
  return porteFantomes;
}

Objet * PorteFantomes::getContenant(){
}

void PorteFantomes::afficherTerminal(){
    cout<<"-";
}
