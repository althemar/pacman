#include "Portail.hpp"

using namespace std;

Portail::Portail(int tailleCase)
  :  Case(tailleCase)
{}

Portail::Portail(int tailleCase, int posTableauX, int posTableauY)
  : Case(tailleCase, posTableauX, posTableauY)
{}

Portail::~Portail(){}

bool Portail::isMur(){
  return false;
}

TypeCase Portail::getType(){
  return portail;
}

Objet * Portail::getContenant(){
  return NULL;
}

void Portail::afficherTerminal(){
    cout<<"p";
}
