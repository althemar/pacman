
#include <iostream>

#include "Entite.hpp"

using namespace std;


Entite::Entite(int vitesse, Case* positionCase, Sens direction){
	this->avancement = positionCase->getTailleCase() / 2;
	this->positionCase = positionCase;
	calculPos();
	/*this->posX = positionCase->getTailleCase() * positionCase->getPosTableauY();
	  this->posY = positionCase->getTailleCase() * positionCase->getPosTableauX();*/
	this->vitesse = vitesse;

	Sens directionEntier = direction;
	this->directionActuelle = directionEntier;
	this->prochaineDirection = directionEntier;
	this->directionDepart = directionEntier;
	this->caseDepart = positionCase;
}

Entite::~Entite(){
}

void Entite::changerDirection(Sens nDirection){
	avancement = positionCase->getTailleCase() / 2;
	directionActuelle = nDirection;
	prochaineDirection = nDirection;
}

void Entite::testerChangementDirection(bool changementVertical){
  bool changementFait = false;
  if ( changementVertical ){
    if ( prochaineDirection == haut && !(positionCase->getCaseHaut()->isMur()) ){
      changerDirection(haut);
      changementFait = true;
    }
    else if ( prochaineDirection == bas && !positionCase->getCaseBas()->isMur() ){
      changerDirection(bas);
      changementFait = true;
    }
    if(changementFait){
      //if(avancement != positionCase->getTailleCase() / 2){
      avancement = positionCase->getTailleCase() / 2;
      posY = positionCase->getTailleCase() * positionCase->getPosTableauX();
    }

		
  }
  else{
    if ( prochaineDirection == gauche && !(positionCase->getCaseGauche()->isMur()) ){
      changerDirection(gauche);
      changementFait = true;
    }
  
    else if ( prochaineDirection == droite && !positionCase->getCaseDroite()->isMur() ){
      changerDirection(droite);
      changementFait = true;
    }
    if(changementFait){
      //if(avancement != positionCase->getTailleCase() / 2){
      avancement = positionCase->getTailleCase() / 2;
      posX = positionCase->getTailleCase() * positionCase->getPosTableauY();
    }
     
  }
}

bool Entite::deplacement(){
	switch (directionActuelle){
		
		case gauche:
		
			if ( ( positionCase->getCaseGauche()->getType() == passage || positionCase->getCaseGauche()->getType() == portail )  
			  || ( (positionCase->getCaseGauche()->isMur() || positionCase->getCaseGauche()->getType() == porteFantomes) && avancement > positionCase->getTailleCase() / 2 ) 
			  || (positionCase->getType() == maisonFantomes) && !(positionCase->getCaseGauche()->isMur() ) ){
				posX -= vitesse;
				avancement -= vitesse;
			}
			
			if ( avancement <= 0 ){

			  if(positionCase->getType() == portail && positionCase->getCaseGauche()->getType() == portail){
			    int diff = positionCase->getCaseGauche()->getPosTableauY() - positionCase->getPosTableauY();
			    posX +=((diff+1)*positionCase->getTailleCase());
			  }

			  positionCase = positionCase->getCaseGauche();
			  
			  avancement = positionCase->getTailleCase() + avancement;
			  
			  return true;
			}
			else if ( prochaineDirection == droite ){
				directionActuelle = droite;
			}
			else if ( (avancement >= positionCase->getTailleCase() / 2 - vitesse)  &&  (avancement <= positionCase->getTailleCase() / 2 + vitesse) ){
			  //else if ( (avancement == positionCase->getTailleCase() / 2) ){
		
			  testerChangementDirection(1);						
			} 
			break;
		case droite:
		  if ( ( positionCase->getCaseDroite()->getType() == passage || positionCase->getCaseDroite()->getType() == portail) 
		    || ( (positionCase->getCaseDroite()->isMur() || positionCase->getCaseDroite()->getType() == porteFantomes) && avancement < positionCase->getTailleCase() / 2) 
		    || (positionCase->getType() == maisonFantomes) && !(positionCase->getCaseDroite()->isMur() ) ) {
				posX += vitesse;
				avancement += vitesse;
			}			
			if ( avancement >= positionCase->getTailleCase() ){

				if(positionCase->getType() == portail && positionCase->getCaseDroite()->getType() == portail){
					int diff = positionCase->getCaseDroite()->getPosTableauY() + positionCase->getPosTableauY();
					posX -=((diff+1)*positionCase->getTailleCase());
				}

				positionCase = positionCase->getCaseDroite();
				avancement =  avancement - positionCase->getTailleCase();
				return true;
			}
			else if ( prochaineDirection == gauche ){
				directionActuelle = gauche;
			}
			else if ( (avancement >= positionCase->getTailleCase() / 2 - vitesse)  &&  (avancement <= positionCase->getTailleCase() / 2 + vitesse) ){
			  //else if ( (avancement == positionCase->getTailleCase() / 2) ){
			
			  testerChangementDirection(1);						
			}
			break;
		case bas:
			
			if ( ( positionCase->getCaseBas()->getType() == passage || positionCase->getCaseBas()->getType() == portail)  
			  || ( (positionCase->getCaseBas()->isMur() || positionCase->getCaseBas()->getType() == porteFantomes) && avancement < positionCase->getTailleCase() / 2 ) 
			  || (positionCase->getType() == maisonFantomes) && !(positionCase->getCaseBas()->isMur() ) ){
				posY += vitesse;
				avancement += vitesse;
			}			
			if ( avancement > positionCase->getTailleCase() ){

			  if(positionCase->getType() == portail && positionCase->getCaseBas()->getType() == portail){
			    int diff = positionCase->getCaseBas()->getPosTableauX() + positionCase->getPosTableauX();
			    posY -=((diff+1)*positionCase->getTailleCase());
			  }
			  
				positionCase = positionCase->getCaseBas();
				avancement = avancement - positionCase->getTailleCase();
				return true;
			}
			else if ( prochaineDirection == haut ){
				directionActuelle = haut;
			}
			else if ( (avancement >= positionCase->getTailleCase() / 2 - vitesse)  &&  (avancement <= positionCase->getTailleCase() / 2 + vitesse) ){
			  //else if ( (avancement == positionCase->getTailleCase() / 2) ){
		
			    testerChangementDirection(0);						
			}
			break;
		case haut:
			if ( ( positionCase->getCaseHaut()->getType() == passage  || positionCase->getCaseHaut()->getType() == portail)  
			  || ( (positionCase->getCaseHaut()->isMur() || positionCase->getCaseHaut()->getType() == porteFantomes) && avancement > positionCase->getTailleCase() / 2 )  
			  || (positionCase->getType() == maisonFantomes) && !(positionCase->getCaseHaut()->isMur() ) ){
				posY -= vitesse;
				avancement -= vitesse;
			}
			if ( avancement < 0 ){
			  
			  if(positionCase->getType() == portail && positionCase->getCaseHaut()->getType() == portail){
			    int diff = positionCase->getCaseHaut()->getPosTableauX() - positionCase->getPosTableauX();
			    posY +=((diff+1)*positionCase->getTailleCase());
			  }
			  
				positionCase = positionCase->getCaseHaut();
				avancement = positionCase->getTailleCase() + avancement;
				return true;
			}
			else if ( prochaineDirection == bas ){
				directionActuelle = bas;
			}	
			else if ( (avancement >= positionCase->getTailleCase() / 2 - vitesse)  &&  (avancement <= positionCase->getTailleCase() / 2 + vitesse) ){
			//else if ( (avancement == positionCase->getTailleCase() / 2) ){
		
			     testerChangementDirection(0);						
			}
			break;
	}
	return false;	
}

int Entite::getPosX(){
	return posX;
}

int Entite::getPosY(){
	return posY;
}

Sens Entite::getDirectionActuelle(){
	return directionActuelle;
}

Sens Entite::getProchaineDirection(){
	return prochaineDirection;
}

void Entite::setVitesse(int vitesse){
	this->vitesse = vitesse;
}

void Entite::setProchaineDirection(Sens dir){
	prochaineDirection = dir;
}

Case* Entite::getPositionCase(){
	return positionCase;
}

void Entite::calculPos(){
  posX = positionCase->getTailleCase() * positionCase->getPosTableauY();
  posY = positionCase->getTailleCase() * positionCase->getPosTableauX();
}
