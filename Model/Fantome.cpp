#include "Fantome.hpp"
#include <iostream>
#include <cstdlib>

using namespace std;

int convertirEtatFantome(string direction){
	if (direction == "blinky")
		return 0;
	else if (direction == "pinky")
		return 1;
	else if (direction == "inky")
		return 2;
	else if (direction == "clyde")
		return 3;
}
Fantome::Fantome(int vitesse, Case * positionCase, string nom, Sens direction)
 :Entite(vitesse, positionCase, direction){
  this->nomFantome = static_cast<EtatFantome>(convertirEtatFantome(nom));
  this->etat = attente;
	
}

Fantome::Fantome(int vitesse, Case* positionCase, Case* caseSortieMaison, string nom, Sens direction, Pacman* pacman)

	: Entite(vitesse, positionCase, direction)
{
	this->nomFantome = static_cast<EtatFantome>(convertirEtatFantome(nom));
	this->pacman = pacman;
	
	if (positionCase->getType() == maisonFantomes || positionCase->getType() == porteFantomes){
		this->etat = attente;
	}
	else
		this->etat = poursuite;
		
	this->vitesseNormale = vitesse;
	this->vitesseRalentie = 2;
	
	
	this->caseSortieMaison = caseSortieMaison;
	definirCaseCible();
	definirProchaineDirection();
}

Fantome::~Fantome(){	
}

EtatFantome Fantome::getNomFantome(){
	return nomFantome;
}

Case* Fantome::getCaseCible(){
	return caseCible;
}

EtatFantome Fantome::getEtat(){
	return etat;
}

void Fantome::setCaseCible(Case* newCaseCible){
	caseCible = newCaseCible;
}

void Fantome::setEtat(EtatFantome etat){
	this->etat = etat;
}

void Fantome::setVitesseNormale(){
	vitesse = vitesseNormale;
}

void Fantome::setVitesseRalentie(){
	vitesse = vitesseRalentie;
}

void Fantome::definirProchaineDirection(){
	
	int diff;
	int distanceMin = 2 * positionCase->distanceEntreCases(caseCible);
	if (etat == poursuite || etat == retourMaison){
		
			
		if (!positionCase->getCaseGauche()->isMur() && directionActuelle != droite && !(positionCase->getType() == passage && positionCase->getCaseGauche()->getType() == porteFantomes)){
			diff = positionCase->getCaseGauche()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = gauche;
				distanceMin = diff;
			}
		}
		if (!positionCase->getCaseDroite()->isMur() && directionActuelle != gauche && !(positionCase->getType() == passage && positionCase->getCaseDroite()->getType() == porteFantomes)){
			diff = positionCase->getCaseDroite()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = droite;
				distanceMin = diff;
			}
		}
			
		if (!positionCase->getCaseHaut()->isMur() && directionActuelle != bas && !(positionCase->getType() == passage && positionCase->getCaseHaut()->getType() == porteFantomes)){
			diff = positionCase->getCaseHaut()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = haut;
				distanceMin = diff;
			}
		}
		
		if (!positionCase->getCaseBas()->isMur() && directionActuelle != haut && !(positionCase->getType() == passage && positionCase->getCaseBas()->getType() == porteFantomes)){
			diff = positionCase->getCaseBas()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = bas;
				distanceMin = diff;
			}
		}	
	}
	else if (etat == sortMaison){			
		if (!positionCase->getCaseGauche()->isMur() && directionActuelle != droite){
			diff = positionCase->getCaseGauche()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = gauche;
				distanceMin = diff;
			}
		}
		if (!positionCase->getCaseDroite()->isMur() && directionActuelle != gauche){
			diff = positionCase->getCaseDroite()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = droite;
				distanceMin = diff;
			}
		}
		
		if (!positionCase->getCaseHaut()->isMur()){
			diff = positionCase->getCaseHaut()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = haut;
				distanceMin = diff;
			}
		}
		if (!positionCase->getCaseBas()->isMur()){
			diff = positionCase->getCaseBas()->distanceEntreCases(caseCible);
			if (diff <= distanceMin){
				prochaineDirection = bas;
				distanceMin = diff;
			}
		}	
	}
	else if (etat == fuite){
		bool prochaineDirectionValide = 0;
	
		while (!prochaineDirectionValide){
			prochaineDirection = static_cast<Sens>(rand()%4);
			switch (prochaineDirection){
				case droite:
					if (!positionCase->getCaseDroite()->isMur() && directionActuelle!=gauche){
						prochaineDirectionValide = 1;
					}
					break;
				case gauche:
					if (!positionCase->getCaseGauche()->isMur() && directionActuelle!=droite){
						prochaineDirectionValide = 1;	
					}
					break;
				case bas:
					if (!positionCase->getCaseBas()->isMur() && directionActuelle!=haut){
						prochaineDirectionValide = 1;	
					}
					break;	
				case haut:
					if (!positionCase->getCaseHaut()->isMur() && directionActuelle!=bas){
						prochaineDirectionValide = 1;
					}
					break;
			}
		}
	}		
}



void Fantome::definirCaseCible(){
	switch (etat){
		case poursuite:
			switch (nomFantome){
				case blinky:
						caseCible = pacman->getPositionCase();
						break;
				case pinky:
					{
						Case* casePacman = pacman->getPositionCase();
						int posXCase = casePacman->getPosTableauX();
						int posYCase = casePacman->getPosTableauY();					
						int cptCases = 4;
						caseCible = NULL;
							
						while (caseCible == NULL && cptCases != -1){
							caseCible = casePacman;
							int cpt = cptCases;
							while (cpt > 0 && caseCible != NULL){
								switch (pacman->getDirectionActuelle()){
									case droite:
										caseCible = caseCible->getCaseDroite();
										break;
									case gauche:
										caseCible = caseCible->getCaseGauche();
										break;
									case haut:
										caseCible = caseCible->getCaseHaut();
										break;
									case bas:
										caseCible = caseCible->getCaseBas();
								}
								cpt--;
							}
							cptCases--;
						}
						break;
							
					}	
					break;
				case inky:
					caseCible = pacman->getPositionCase();
					break;
				case clyde:
					caseCible = pacman->getPositionCase();
					break;
			}
			break;
		case sortMaison:
			caseCible = caseSortieMaison;
			break;
		case retourMaison:
			caseCible = caseSortieMaison;
			break;
		case attente:
			caseCible = positionCase;
			break;
	}		
}


void Fantome::deplacer(){
	if (etat != attente){
		if (deplacement() ){
			if (positionCase->isIntersection()){
				if (positionCase->getType() == porteFantomes && etat == sortMaison){
					etat = poursuite;
				}
				
				if (etat == retourMaison){
					if (positionCase == caseSortieMaison->getCaseHaut()){
						etat = poursuite;
						vitesse = vitesseNormale;
					}
				}
				
				definirCaseCible();
				definirProchaineDirection();
				if (caseCible == positionCase){
					caseCible = pacman->getPositionCase();
					definirProchaineDirection();
				}
				
			}
		}
	}
}


void Fantome::reinitialiserPos(){
	positionCase = caseDepart;
	vitesse = vitesseNormale;
	avancement = positionCase->getTailleCase() / 2;
	calculPos();
	if (positionCase->getType() == maisonFantomes)
		etat = attente;
	else
		etat = poursuite;
	
	definirCaseCible();
	//cout << caseCible->getPosTableauX() << " " << caseCible->getPosTableauY() << endl;
	definirProchaineDirection();
	//cout << prochaineDirection << endl;
}
