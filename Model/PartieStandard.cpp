#include "PartieStandard.hpp"

using namespace std;

PartieStandard::PartieStandard(string nomFichier)
	: EtatProgramme(){
	etat = pause;
	plateau = new Plateau(nomFichier);
	niveau = 1;
	cptFrameFantomes = 0;
}

PartieStandard::~PartieStandard(){
	delete plateau;
}

EtatPartie PartieStandard::getEtat(){
	return etat;
}

int PartieStandard::getNiveau(){
	return niveau;
}
void PartieStandard::setEtat(EtatPartie etat){
	this->etat = etat;
}
void PartieStandard::update()
{
	if (etat != pause)
		cptFrameFantomes++;
		
	plateau->getPacman()->deplacer();
	
	if (plateau->getPacman()->getCptInvincible() == plateau->getPacman()->getTmpInvincible()){
		for (int i = 0; i < plateau->getFantomes().size(); i++){
			if (plateau->getFantomes()[i]->getEtat() == poursuite){
				plateau->getFantomes()[i]->setEtat(fuite);
				plateau->getFantomes()[i]->setVitesseRalentie();
			}
		}
	}
	
	if (plateau->getPacman()->getEtat() == invincible){
		plateau->getPacman()->decreaseCptInvincible();
		if (plateau->getPacman()->getCptInvincible() == 0){
			for (int i = 0; i < plateau->getFantomes().size(); i++){
				if (plateau->getFantomes()[i]->getEtat() == fuite){
					plateau->getFantomes()[i]->setEtat(poursuite);
					plateau->getFantomes()[i]->setVitesseNormale();
				}
			}
		}
	}
	
				
	


	if(plateau->nbPacgum() < (plateau->getLargeur()*plateau->getHauteur())/3 && plateau->getCaseFruit()==NULL)
		plateau->placerFruit(getTypeFruit());

	if( cptFrameFantomes == 300  && plateau->getNbFantomesDansMaison() >= 1)
		plateau->libererFantome();
	if( cptFrameFantomes == 600 && plateau->getNbFantomesDansMaison() >= 1)
		plateau->libererFantome();
	if( cptFrameFantomes == 900 && plateau->getNbFantomesDansMaison() >= 1)
		plateau->libererFantome();
	
  
	if(plateau->nbPacgum() %50 == 0 && plateau->getCaseFruit()==NULL)
		plateau->placerFruit(getTypeFruit());
  
	for (int i = 0; i < plateau->getFantomes().size(); i++){	
		plateau->getFantomes()[i]->deplacer();
	}
  
	if (!plateau->verifierCollisionsFantomes()){
		cptFrameFantomes = 0;
		etat = pause;
	}
  
	if(plateau->nbPacgum()==0){
		etat = gagne;
	}

	if(plateau->getPacman()->getVie()<=0){
		etat = perdu;
		cout<<"C'est perdu !"<<endl;
	}

}

TypeObjet PartieStandard::getTypeFruit(){
	if(niveau == 1)
		return cerise;
	if(niveau == 2)
		return fraise;
	if(niveau == 3 || niveau == 4)
		return orange;
	if(niveau == 5 || niveau == 6)
		return pomme;
	if(niveau == 7 || niveau == 8)
		return melon;
	if(niveau == 9 || niveau == 10)
		return galboss;
	if(niveau == 11 || niveau == 12)
		return cloche;
	if(niveau >=13)
		return cle;
}

void PartieStandard::niveauSuivant(){
  int score = plateau->getScore();
  int vie = plateau->getPacman()->getVie();
  cptFrameFantomes = 0;
  delete plateau;
  niveau ++;

  plateau = new Plateau("OriginalMap.txt");
  plateau->setScore(score);
  plateau->getPacman()->setVie(vie);

}
