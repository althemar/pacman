#include "Case.hpp"

using namespace std;

Case::Case(int tailleCase){
  this->tailleCase = tailleCase;
  angle = 0;
  initTabAdjMurs();
  initTabDiagoMurs();
}

Case::Case(int tailleCase, int posTableauX, int posTableauY){
  angle=0;
  this->tailleCase = tailleCase;
  this->posTableauX = posTableauX;
  this->posTableauY = posTableauY;
  caseGauche = caseDroite = caseHaut = caseBas = NULL;
  initTabAdjMurs();
  initTabDiagoMurs();
}

Case::~Case(){
}

void Case::initTabAdjMurs(){
  tabAdjMurs[0]=tabAdjMurs[1]=tabAdjMurs[2]=tabAdjMurs[3]=0;
}

void Case::initTabDiagoMurs(){
tabDiagoMurs[0]=tabDiagoMurs[1]=tabDiagoMurs[2]=tabDiagoMurs[3]=0;
}

Case * Case::getCaseGauche(){
  return caseGauche;
}

Case * Case::getCaseDroite(){
  return caseDroite;
}

Case * Case::getCaseHaut(){
  return caseHaut;
}

Case * Case::getCaseBas(){
  return caseBas;
}

int Case::getTailleCase(){
  return tailleCase;
}

void Case::setTailleCase(int taille){
  tailleCase = taille;
}

int Case::getAngle(){
  return angle;
}

void Case::setCaseGauche(Case * c){
  caseGauche = c;
}

void Case::setCaseDroite(Case * c){
  caseDroite = c;
}

void Case::setCaseHaut(Case * c){
  caseHaut = c;
}

void Case::setCaseBas(Case * c){
  caseBas = c;
}

int Case::getPosTableauX(){
	return posTableauX;
}

int Case::getPosTableauY(){
	return posTableauY;
}

bool * Case::getTabAdjMurs(){
  initTabAdjMurs();
  tabAdjMurs[0]=isMurGauche();
  tabAdjMurs[1]=isMurDroite();
  tabAdjMurs[2]=isMurHaut();
  tabAdjMurs[3]=isMurBas();
  return tabAdjMurs;
}

bool * Case::getTabDiagoMurs(){
  initTabDiagoMurs();
  if (isMurGauche() && isMurHaut())
    tabDiagoMurs[0]=getCaseGauche()->isMurHaut();
  if (isMurHaut() && isMurDroite())
    tabDiagoMurs[1]=getCaseHaut()->isMurDroite();
  if (isMurDroite() && isMurBas())
    tabDiagoMurs[2]=getCaseDroite()->isMurBas();
  if(isMurBas() && isMurGauche())
    tabDiagoMurs[3]=getCaseBas()->isMurGauche();
  return tabDiagoMurs;
}

int Case::nbMurAdj(){
  return tabAdjMurs[0]+tabAdjMurs[1]+tabAdjMurs[2]+tabAdjMurs[3];
}


bool Case::isMurGauche(){
  if(getCaseGauche()==NULL)
    return false;
  else 
    return getCaseGauche()->isMur();
}
bool Case::isMurDroite(){
   if(getCaseDroite()==NULL)
    return false;
  else 
    return getCaseDroite()->isMur();
}

bool Case::isMurHaut(){
  if(getCaseHaut()==NULL)
    return false;
  else 
    return getCaseHaut()->isMur();
}

bool Case::isMurBas(){
  if(getCaseBas()==NULL){
    return false;
  }
  else 
    return getCaseBas()->isMur();
}

bool Case::isIntersection(){
	int passages = 0;
	if (!caseBas->isMur())
		passages++;
	if (!caseDroite->isMur())
		passages++;
	if (!caseGauche->isMur())
		passages++;
	if (!caseHaut->isMur())
		passages++;
		
	if (passages >= 2)
		return true;
	else
		return false;
}

int Case::distanceEntreCases(Case* case2){
	int diff;
	if (case2->getPosTableauX() >= posTableauX && case2->getPosTableauY() >= posTableauY){
		diff = ( case2->getPosTableauX() - posTableauX ) + ( case2->getPosTableauY() - posTableauY );
	}
	else if (case2->getPosTableauX() >= posTableauX && case2->getPosTableauY() < posTableauY){
		diff = ( case2->getPosTableauX() - posTableauX ) + ( posTableauY - case2->getPosTableauY() );
	}
	else if (case2->getPosTableauX() < posTableauX && case2->getPosTableauY() >= posTableauY){
		diff = ( posTableauX - case2->getPosTableauX() ) + ( case2->getPosTableauY() - posTableauY );
	}
	else if (case2->getPosTableauX() < posTableauX && case2->getPosTableauY() < posTableauY){
		diff = ( posTableauX - case2->getPosTableauX() ) + ( posTableauY - case2->getPosTableauY() );
	}
	return diff;
}

void Case::mettreAJour(int tailleCase, int posX, int posY){
  this->tailleCase = tailleCase;
  posTableauX = posX;
  posTableauY = posY;
}

