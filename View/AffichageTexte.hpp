#ifndef AFFICHAGE_TEXTE
#define AFFICHAGE_TEXTE

#include <string>
#include "AffichageClasse.hpp"


class AffichageTexte : public AffichageClasse{

  std::string texte;
  TTF_Font * police;
  

public:
  AffichageTexte();
  AffichageTexte(int largeur, int hauteur, int posX, int posY, TTF_Font * police, std::string texte);

  void afficher(SDL_Renderer * renderer);

  void setTexte(std::string texte);

  ~AffichageTexte();
};

#endif
