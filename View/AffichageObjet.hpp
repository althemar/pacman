#ifndef AFFICHAGE_OBJET
#define AFFICHAGE_OBJET

#include <SDL2/SDL.h>

#include "AffichageClasse.hpp"
#include "../Model/Objet.hpp"
#include "../Model/Passage.hpp"

class AffichageObjet : public AffichageClasse{
	
private:

  Case * passage; 
  static mapTexture textures;
  
  
  	
public:
  
  AffichageObjet();
  
  AffichageObjet(int pos_x, int pos_y, int largeur, int hauteur, Case * passage);
  ~AffichageObjet();
  
  void ajouterTexture(std::string src,int type, SDL_Renderer* renderer);
  
  void afficher(SDL_Renderer* renderer);

  static void chargerToutesTextures(SDL_Renderer * renderer);

  static SDL_Texture * getTexture(int type);
  void supprimerToutesTextures();
};

#endif
