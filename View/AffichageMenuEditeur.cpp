#include <iostream>
#include <string>
#include "AffichageMenuEditeur.hpp"

using namespace std;

AffichageMenuEditeur::AffichageMenuEditeur(int largeur, int hauteur, int posX, int posY)
  : ZoneEcran(largeur, hauteur, posX, posY)
{
  chargerPolice();
  affichageTitre = new AffichageTexte(4*largeur/5, hauteur/20, posX+5, posY+10, police, "Editeur");
  
  

  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonModeMur, new AffichageBouton(4*largeur/5, hauteur/10, posX + 2*hauteur/20, posY+10, police, "Mode Mur", normal)));
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonModePassage, new AffichageBouton(4*largeur/5, hauteur/10, posX + 4*hauteur/20, posY+10, police, "Mode Passage", normal)));
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonModePortail, new AffichageBouton(4*largeur/5, hauteur/10, posX + 6*hauteur/20, posY+10, police, "Mode Portail", normal)));
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonModePacman, new AffichageBouton(4*largeur/5, hauteur/10, posX + 8*hauteur/20, posY+10, police, "Mode Pacman", normal)));
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonModeFantome, new AffichageBouton(4*largeur/5, hauteur/10, posX + 10*hauteur/20, posY+10, police, "Mode Fantome", normal)));
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonSauvegarder, new AffichageBouton(4*largeur/5, hauteur/10, posX + 15*hauteur/20, posY+10, police, "Sauvegarder", normal)));
  
  boutons.push_back(pair<TypeBouton, AffichageBouton*> (boutonAccueil, new AffichageBouton(4*largeur/5, hauteur/10, H_ECRAN - 10 - hauteur/10, posY+10, police, "Retourner Accueil", normal)));
  
}

void AffichageMenuEditeur::afficher(SDL_Renderer * renderer){
  affichageTitre->afficher(renderer);

  for(int i = 0; i <boutons.size();i++)
    boutons[i].second->afficher(renderer);
  
}

void AffichageMenuEditeur::chargerPolice(){
  police = TTF_OpenFont("Ressources/Fonts/alterebro-pixel-font.ttf", 30);
  if(!police)
    printf("TTF_OpenFront: %s\n", TTF_GetError()); 
}



vector<pair<TypeBouton, AffichageBouton *> > AffichageMenuEditeur::getBoutons(){
  return boutons;
}

AffichageMenuEditeur::~AffichageMenuEditeur(){
  delete affichageTitre;
  
  if(police!=NULL)
    TTF_CloseFont(police);
  police=NULL;
 
  for(int i = 0; i <boutons.size();i++)
    delete boutons[i].second;
  
}


string AffichageMenuEditeur::intToString(int i){
  stringstream out;
  out<<i;
  return out.str();
}
