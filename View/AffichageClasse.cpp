#include "AffichageClasse.hpp"


#include <SDL2/SDL_image.h>

using namespace std;


AffichageClasse::AffichageClasse(){
  rect = NULL;
}

AffichageClasse::AffichageClasse(int largeur, int hauteur)
{		
	this->rect = new SDL_Rect();
	this->rect->w = largeur;
	this->rect->h = hauteur;
}

AffichageClasse::~AffichageClasse(){

  if(rect)
    delete rect;
}

SDL_Texture* AffichageClasse::chargerTexture(string src, SDL_Renderer* renderer){
	
	SDL_Texture* newTexture = NULL;
	SDL_Surface* loadedSurface = IMG_Load(src.c_str());
	
	if (loadedSurface == NULL){
		printf("Unable to load image %s! SDL_image Error: %s\n", src.c_str(), IMG_GetError());
	}
	else{
		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
		if (newTexture == NULL){
			printf("Unable to create texture from %s! SDL Error: %s\n", src.c_str(), SDL_GetError());
		}	
	  
		SDL_FreeSurface(loadedSurface);
	}
	return newTexture;
}

