#ifndef AFFICHAGE_EDITEUR
#define AFFICHAGE_EDITEUR

#include "AffichageZoneEdition.hpp"
#include "AffichageMenuEditeur.hpp"
#include "../Controller/ControllerEditeur.hpp"
#include "../Model/Editeur.hpp"
#include "ZoneEcran.hpp"

class AffichageEditeur : public ZoneEcran{

  Editeur * editeur;
  AffichageZoneEdition * affichageZoneEdition;
  AffichageMenuEditeur * affichageMenuEditeur;
  ControllerEditeur * controllerEditeur;

public :
  AffichageEditeur(int largeur, int hauteur, int posX, int posY);
  
  void afficher(SDL_Renderer * renderer);

  ~AffichageEditeur();

};



#endif
