#include "ControllerPartie.hpp"

#include <iostream>

using namespace std;


ControllerPartie::ControllerPartie(PartieStandard* partieStandard)
  : Controller(){
  this->partieStandard = partieStandard;
  etat = controlPartie;

  
}

void ControllerPartie::gererEvenements(){
  
  framerateControl++;
  SDL_PollEvent(event);
  for(map<TypeBouton, AffichageBouton*>::iterator it = mapBoutons.begin(); 
      it != mapBoutons.end(); it++)
    if(it->second->inBouton(event->motion.x, event->motion.y)){
      if (it->second->getEtat() == normal)
	it->second->setEtat(survol);
    }
    else 
      if(it->second->getEtat() == survol)
	it->second->setEtat(normal);
  
  
  switch (event->type){
    
  case SDL_QUIT:
    etat = fermer;
    break;
    
  case SDL_MOUSEBUTTONDOWN:
    if(framerateControl>10){
 
      framerateControl=0;
      
    
      for(map<TypeBouton, AffichageBouton*>::iterator it = mapBoutons.begin(); 
	  it != mapBoutons.end(); it++){
	
	if(it->second->inBouton(event->motion.x, event->motion.y)){
	  activerBouton(it->first);
	}
      }
    }
    
    break;
  case SDL_KEYDOWN:
    switch (event->key.keysym.sym) {
    case SDLK_UP:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(haut);
      break;
      
    case SDLK_z:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(haut);
      break;
      
    case SDLK_DOWN:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(bas);
      break;
      
    case SDLK_s:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(bas);
      break;
      
    case SDLK_LEFT:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(gauche);
      break;
      
      case SDLK_q:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(gauche);
      break;
      
    case SDLK_RIGHT:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(droite);
      break;
      
      case SDLK_d:
      partieStandard->getPlateau()->getPacman()->setProchaineDirection(droite);
      break;
      
    case SDLK_SPACE:
      if(framerateControl>10){
	framerateControl=0;
	if(partieStandard->getEtat()==en_cours){
	  partieStandard->setEtat(pause);
	  mapBoutons[boutonPause]->setEtat(enfonce);
	}      
	else 
	  if(partieStandard->getEtat()==pause){
	    partieStandard->setEtat(en_cours);
	    mapBoutons[boutonPause]->setEtat(normal);
	  }
      }
      break;
    default:
      break;
    }
    break;		
  }
    
  
  
}

void ControllerPartie::activerBouton(const TypeBouton type){

  switch(type){

  case boutonPause:
    if(partieStandard->getEtat()==en_cours || partieStandard->getEtat()==pause){
      if (mapBoutons[type]->getEtat() == enfonce){
	mapBoutons[type]->setEtat(survol);
	partieStandard->setEtat(en_cours);
      }
      else
	if (mapBoutons[type]->getEtat() == survol){
	  mapBoutons[type]->setEtat(enfonce);
	  partieStandard->setEtat(pause);
	}
    }
    break;
    
    
 case boutonAccueil:
   if (mapBoutons[type]->getEtat() == survol){
     mapBoutons[type]->setEtat(enfonce);
     etat = controlAccueil;
   }
   break;
  }
}
  
